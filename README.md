# Mars Rover

## Getting started

Make sure ruby is available to your console, if it is not you can try the following:

For Mac (although most machines with have ruby 2.x.x installed already):

```
brew install ruby
```

For windows use the linux subsystem or RubyInstaller.

For other Unix-like operating systems use your package manager.

For more details visit https://www.ruby-lang.org/en/documentation/installation/

## How to run the script

The script takes a single argument of a plain text input file:

```
./rover-v1.rb <inputfile>
```

The final positions of the rovers will be outputted into the console

## Future work

# V1

By upgrading to ruby 3.x.x I would have been able to use typing on the method
parameters which would increase the robustness of the inputs. At the moment, for
example I am relying on some built in functionality which does behind the scenes
conversions. Currently (1, Banana, N) -> (1, 0, N).

I would like to also add a better way of escaping a bad initial direction as I
currently set it to north by default if its not recognised.

# V2

In v2 I would like to run each rover simultaneously and check for collisions.
This could be approached from multiple angles:

# Keep v1 processing logic

Slightly modify v1 to record the full list of its statuses (coordinates + direction)
then once all rovers are complete iterate through the coordinate lists looking for
matches then eliminate the pair including the mid points between each pair of coordinates

| Rover 1 | Rover 2 | Rover 3 | Rover 4 | Rover 5 |
| ------- | ------- | ------- | ------- | ------- |
| 1, 1    | 2, 2    | 4, 1    | 4, 2    | 3, 1    |
| 1, 2 X  | 1, 2 X  | 4, 2    | 3, 2    | 4, 1    |
| (1, 3)  | (0, 2)  | 3, 2 X  | 4, 2 X  | 5, 1    |
| (1, 4)  | (0, 3)  |         |         | 5, 2    |
|         | (1, 3)  |         |         | 5, 3    |
|         |         |         |         | 5, 4    |

This would output something along the lines of:

```
(1, 2, N) COLLIDED
(1, 2, W) COLLIDED
(3.5, 2, W) COLLIDED
(3.5, 2, E) COLLIDED
(5, 4, N)
```

# Process by time rather than rover

Another approach would be to keep a collection of rovers and iterate over 'time'.
This would be done by adding the instructions as a property of the Rover class
and creating a new class method to increment over them. This way only the current
(and last positions) of each rover is compared rather than keeping a stored list
of statuses.
