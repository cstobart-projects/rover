#!/usr/bin/env ruby

DIRECTIONS = %w[n e s w].freeze

class Rover
  # The Rover class is instantiated from a line from the input file.
  # attr_readers can be defined to access the properties during runtime
  # from a debugger

  def initialize(starting_x:, starting_y:, starting_direction:, boundaries:)
    @x_position = starting_x
    @y_position = starting_y
    # This converts the alphabetic direction into a numeric value if it exists
    @direction = DIRECTIONS.include?(starting_direction) ? DIRECTIONS.index(starting_direction) : 0
    @boundaries = boundaries
  end

  def turn_left
    @direction -= 1
  end
  alias :l :turn_left

  def turn_right
    @direction += 1
  end
  alias :r :turn_right

  def move_forward
    # By using '%4' we ensure that the DIRECTIONS array is essentially cyclical
    case @direction%4
    when 0 # North
      @y_position += 1
    when 1 # East
      @x_position += 1
    when 2 # South
      @y_position -= 1
    when 3 # West
      @x_position -= 1
    end
  end
  alias :f :move_forward

  def status
    "(#{@x_position}, #{@y_position}, #{DIRECTIONS[@direction%4]})".upcase
  end

  def drive(instructions)
    instructions.downcase.each_byte do |instruction|
      buffered_status = status

      begin
        # This, combinded with the aliases allows each letter of the instructions
        # to be sent to the Rover class as a method. Using ':each_byte' and ':chr'
        # is a hack to split up a string into component letters by creating an array
        # of bytes then converting them back into characters. ':to_sym' converts
        # the object that called on into a symbol (method) and 'send' is a way to
        # call a method that has an unknown name.
        self.send(instruction.chr.to_sym)
      rescue NoMethodError
        # Because of the nature of ':to_sym', the function is protected from typos
        next
      end

      return buffered_status + " LOST" if lost?
    end
    return status
  end

  def lost?
    !@x_position.between?(0, @boundaries[:x]) || !@y_position.between?(0, @boundaries[:y])
  end
end

# Script begins here

begin
  file = File.open ARGV[0]
rescue Errno::ENOENT
  puts 'File not found'
else
  lines = file.read.split "\n"

  boundaries = {'x': lines[0].split(" ")[0].to_i, 'y': lines[0].split(" ")[1].to_i}

  lines.drop(1).each do |line|
    # line ~ ("x", "y", "d") XX... - The first bracket is split then dropped
    # followed by a split on the closing parenthesis.
    setup, instructions = line.split("(").drop(1)[0].split(")")

    # setup ~ ["x", " y", " d"]
    x, y, direction = setup.split(",")

    rover = Rover.new(
      starting_x: x.to_i,
      starting_y: y.to_i,
      starting_direction: direction.strip.downcase,
      boundaries: boundaries
    )

    puts rover.drive(instructions.strip)
  end
end
